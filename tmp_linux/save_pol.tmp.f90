












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Print out all TDLDA eigenvectors. Relevant for debugging purposes
! only.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dsave_pol(pol,iunit,irp,iq)

  use typedefs
  use mpi_module
  implicit none

  ! arguments
  type (polinfo), intent(in) :: pol
  ! number of output unit (local variable)
  integer, intent(in) :: iunit
  ! index of current representation
  integer, intent(in) :: irp
  ! q-vector for which eigenvectors are printed out
  integer, intent(in) :: iq

  ! local variables
  integer :: jtape, irow, icol, pcol

  jtape = iunit + peinf%inode
  do pcol = 1, pol%nn
     icol = pcol + r_grp%inode*pol%nn
     if (icol > pol%ntr) cycle
     do irow = 1, pol%ntr
        write(jtape,'(2i2,2i5,8i4,2e18.6)') iq, irp, irow, icol, &
             pol%tr(:,irow), pol%tr(:,icol), &
             pol%dv(irow,pcol)
     enddo
  enddo
  call flush(jtape)
  return

end subroutine dsave_pol
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Print out all TDLDA eigenvectors. Relevant for debugging purposes
! only.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zsave_pol(pol,iunit,irp,iq)

  use typedefs
  use mpi_module
  implicit none

  ! arguments
  type (polinfo), intent(in) :: pol
  ! number of output unit (local variable)
  integer, intent(in) :: iunit
  ! index of current representation
  integer, intent(in) :: irp
  ! q-vector for which eigenvectors are printed out
  integer, intent(in) :: iq

  ! local variables
  integer :: jtape, irow, icol, pcol

  jtape = iunit + peinf%inode
  do pcol = 1, pol%nn
     icol = pcol + r_grp%inode*pol%nn
     if (icol > pol%ntr) cycle
     do irow = 1, pol%ntr
        write(jtape,'(2i2,2i5,8i4,2e18.6)') iq, irp, irow, icol, &
             pol%tr(:,irow), pol%tr(:,icol), &
             real(pol%zv(irow,pcol),dp), aimag(pol%zv(irow,pcol))
     enddo
  enddo
  call flush(jtape)
  return

end subroutine zsave_pol
!===================================================================
