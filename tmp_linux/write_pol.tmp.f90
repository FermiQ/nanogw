












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===============================================================
!
! Write eigenvectors,eigenvalues to file pol_diag.dat : master PE
! receives data from all other PEs and writes to disk, one full
! eigenvector per line.
! 
! At output, pol%dv is deallocated and pol%lv is set to false. No
! other variables are modified.
! 
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dwrite_pol(pol,nrep,nq,irp,iq,nstate)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'
  ! arguments
  ! polarizability for this representation and q-vector
  type (polinfo), intent(inout) :: pol
  integer, intent(in) :: &
       nrep, &      ! number of representations
       nq,  &       ! number of q-vectors
       irp,  &      ! index of current representation
       iq,  &       ! index of current q-vector
       nstate       ! total number of electronic orbitals

  ! local variables
  integer, parameter :: iunit = 50  ! number of input/output unit
  ! date tag
  character (len=26) :: datelabel
  ! error message
  character (len=800) :: lastwords
  ! counters and temporary variables
  integer :: ii, jj, ipe, ncol, info, nline
  real(dp), allocatable ::  tdummy(:,:)
  integer :: jpe
  integer :: status(MPI_STATUS_SIZE)

  !-------------------------------------------------------------------
  ! Master PE scans existing pol_diag.dat file to the end and writes
  ! some data at the end.
  !
  if (peinf%master) then
     open(iunit,file='pol_diag.dat',form='unformatted',iostat=info)
     if (info /= 0) then
        call die('Could not open output file pol_diag.dat')
     endif
     read(iunit,end=15)
     read(iunit,end=15)
     nline = 2
     do
        read(iunit,end=20) jj, ii, ncol
        if (jj == irp .and. ii == iq) then
           write(lastwords,*) ' Representation ',irp,' q-vector ',iq, &
                ' already present in pol_diag.dat'
           call die(lastwords)
        endif
        do jj = 1, ncol + 2
           read(iunit)
        enddo
        nline = nline + 3 + ncol
     enddo
15   continue
     rewind(iunit)
     call get_date(datelabel)
     write(iunit) datelabel
     ii = 1
     write(iunit) nstate, nrep, nq, ii
     nline = 2
20   continue
     rewind(iunit)
     do jj = 1, nline
        read(iunit)
     enddo
     write(iunit) irp, iq, pol%ntr, pol%ntr, pol%n_up
     write(iunit) (pol%tr(:,jj),jj=1,pol%ntr)
  endif
  !-------------------------------------------------------------------
  ! If master does not have this representation, the group master must
  ! transfer data to master PE.
  !
  ipe = r_grp%map(r_grp%masterid+1,r_grp%rep_g(irp)+1)
  if (ipe /= peinf%masterid) then
     if (r_grp%master .and. r_grp%mygr == r_grp%rep_g(irp)) &
          call MPI_SEND(pol%nn,1, &
          MPI_INTEGER,peinf%masterid,ipe,peinf%comm,info)
     if (peinf%master .and. r_grp%mygr /= r_grp%rep_g(irp)) &
          call MPI_RECV(pol%nn,1, &
          MPI_INTEGER,ipe,ipe,peinf%comm,status,info)
     if (r_grp%master .and. r_grp%mygr == r_grp%rep_g(irp)) then
        call MPI_SEND(pol%eig,pol%ntr, &
          MPI_DOUBLE_PRECISION,peinf%masterid,ipe+1,peinf%comm,info)
        call MPI_SEND(pol%ostr,pol%ntr*3, &
          MPI_DOUBLE_PRECISION,peinf%masterid,ipe+2,peinf%comm,info)
     endif
     if (peinf%master .and. r_grp%mygr /= r_grp%rep_g(irp)) then
        call MPI_RECV(pol%eig,pol%ntr, &
          MPI_DOUBLE_PRECISION,ipe,ipe+1,peinf%comm,status,info)
        call MPI_RECV(pol%ostr,pol%ntr*3, &
          MPI_DOUBLE_PRECISION,ipe,ipe+2,peinf%comm,status,info)
     endif
  endif

  if (peinf%master) then
     allocate(tdummy(pol%nn*r_grp%npes,pol%nn))
     write(iunit) (pol%eig(jj),jj=1,pol%ntr)
  endif
  if (r_grp%master) write(6,*) ' EIGENVALUES ', &
       minval(pol%eig), maxval(pol%eig), sum(pol%eig), associated(pol%dv)

  ncol = pol%nn
  do ipe = 0, r_grp%npes - 1
     if (pol%ntr < ncol + ipe*pol%nn) ncol = pol%ntr - ipe*pol%nn
     if (peinf%master .and. r_grp%rep_g(irp) == r_grp%mygr) tdummy = pol%dv
     jpe = r_grp%map(ipe+1,r_grp%rep_g(irp)+1)
     if (peinf%inode == jpe .and. (.not. peinf%master)) &
          call MPI_SEND(pol%dv,pol%nn*r_grp%npes*pol%nn, &
          MPI_DOUBLE_PRECISION,peinf%masterid,jpe,peinf%comm,info)
     if (peinf%inode /= jpe .and. peinf%master) &
          call MPI_RECV(tdummy,pol%nn*r_grp%npes*pol%nn, &
          MPI_DOUBLE_PRECISION,jpe,jpe,peinf%comm,status,info)
     if (peinf%master) then
        do jj = 1, ncol
           write(iunit) (tdummy(ii,jj),ii=1,pol%ntr)
           do ii = 1, pol%ntr
              write(8000,'(2i2,2i5,8i4,2e18.6)') iq, irp, ii,jj, &
                   pol%tr(:,ii), pol%tr(:,jj), &
                   tdummy(ii,jj)
           enddo
        enddo
     endif
     call MPI_BARRIER(peinf%comm,info)
  enddo
  if (peinf%master) then
     close(iunit)
     deallocate(tdummy)
  endif

  if (r_grp%rep_g(irp) == r_grp%mygr) then
     pol%lv = .false.
     deallocate(pol%dv)
  endif

  call MPI_BARRIER(peinf%comm,info)

  return

end subroutine dwrite_pol
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===============================================================
!
! Write eigenvectors,eigenvalues to file pol_diag.dat : master PE
! receives data from all other PEs and writes to disk, one full
! eigenvector per line.
! 
! At output, pol%zv is deallocated and pol%lv is set to false. No
! other variables are modified.
! 
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zwrite_pol(pol,nrep,nq,irp,iq,nstate)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'
  ! arguments
  ! polarizability for this representation and q-vector
  type (polinfo), intent(inout) :: pol
  integer, intent(in) :: &
       nrep, &      ! number of representations
       nq,  &       ! number of q-vectors
       irp,  &      ! index of current representation
       iq,  &       ! index of current q-vector
       nstate       ! total number of electronic orbitals

  ! local variables
  integer, parameter :: iunit = 50  ! number of input/output unit
  ! date tag
  character (len=26) :: datelabel
  ! error message
  character (len=800) :: lastwords
  ! counters and temporary variables
  integer :: ii, jj, ipe, ncol, info, nline
  complex(dpc), allocatable ::  tdummy(:,:)
  integer :: jpe
  integer :: status(MPI_STATUS_SIZE)

  !-------------------------------------------------------------------
  ! Master PE scans existing pol_diag.dat file to the end and writes
  ! some data at the end.
  !
  if (peinf%master) then
     open(iunit,file='pol_diag.dat',form='unformatted',iostat=info)
     if (info /= 0) then
        call die('Could not open output file pol_diag.dat')
     endif
     read(iunit,end=15)
     read(iunit,end=15)
     nline = 2
     do
        read(iunit,end=20) jj, ii, ncol
        if (jj == irp .and. ii == iq) then
           write(lastwords,*) ' Representation ',irp,' q-vector ',iq, &
                ' already present in pol_diag.dat'
           call die(lastwords)
        endif
        do jj = 1, ncol + 2
           read(iunit)
        enddo
        nline = nline + 3 + ncol
     enddo
15   continue
     rewind(iunit)
     call get_date(datelabel)
     write(iunit) datelabel
     ii = 2
     write(iunit) nstate, nrep, nq, ii
     nline = 2
20   continue
     rewind(iunit)
     do jj = 1, nline
        read(iunit)
     enddo
     write(iunit) irp, iq, pol%ntr, pol%ntr, pol%n_up
     write(iunit) (pol%tr(:,jj),jj=1,pol%ntr)
  endif
  !-------------------------------------------------------------------
  ! If master does not have this representation, the group master must
  ! transfer data to master PE.
  !
  ipe = r_grp%map(r_grp%masterid+1,r_grp%rep_g(irp)+1)
  if (ipe /= peinf%masterid) then
     if (r_grp%master .and. r_grp%mygr == r_grp%rep_g(irp)) &
          call MPI_SEND(pol%nn,1, &
          MPI_INTEGER,peinf%masterid,ipe,peinf%comm,info)
     if (peinf%master .and. r_grp%mygr /= r_grp%rep_g(irp)) &
          call MPI_RECV(pol%nn,1, &
          MPI_INTEGER,ipe,ipe,peinf%comm,status,info)
     if (r_grp%master .and. r_grp%mygr == r_grp%rep_g(irp)) then
        call MPI_SEND(pol%eig,pol%ntr, &
          MPI_DOUBLE_PRECISION,peinf%masterid,ipe+1,peinf%comm,info)
        call MPI_SEND(pol%ostr,pol%ntr*3, &
          MPI_DOUBLE_PRECISION,peinf%masterid,ipe+2,peinf%comm,info)
     endif
     if (peinf%master .and. r_grp%mygr /= r_grp%rep_g(irp)) then
        call MPI_RECV(pol%eig,pol%ntr, &
          MPI_DOUBLE_PRECISION,ipe,ipe+1,peinf%comm,status,info)
        call MPI_RECV(pol%ostr,pol%ntr*3, &
          MPI_DOUBLE_PRECISION,ipe,ipe+2,peinf%comm,status,info)
     endif
  endif

  if (peinf%master) then
     allocate(tdummy(pol%nn*r_grp%npes,pol%nn))
     write(iunit) (pol%eig(jj),jj=1,pol%ntr)
  endif
  if (r_grp%master) write(6,*) ' EIGENVALUES ', &
       minval(pol%eig), maxval(pol%eig), sum(pol%eig), associated(pol%zv)

  ncol = pol%nn
  do ipe = 0, r_grp%npes - 1
     if (pol%ntr < ncol + ipe*pol%nn) ncol = pol%ntr - ipe*pol%nn
     if (peinf%master .and. r_grp%rep_g(irp) == r_grp%mygr) tdummy = pol%zv
     jpe = r_grp%map(ipe+1,r_grp%rep_g(irp)+1)
     if (peinf%inode == jpe .and. (.not. peinf%master)) &
          call MPI_SEND(pol%zv,pol%nn*r_grp%npes*pol%nn, &
          MPI_DOUBLE_COMPLEX,peinf%masterid,jpe,peinf%comm,info)
     if (peinf%inode /= jpe .and. peinf%master) &
          call MPI_RECV(tdummy,pol%nn*r_grp%npes*pol%nn, &
          MPI_DOUBLE_COMPLEX,jpe,jpe,peinf%comm,status,info)
     if (peinf%master) then
        do jj = 1, ncol
           write(iunit) (tdummy(ii,jj),ii=1,pol%ntr)
           do ii = 1, pol%ntr
              write(8000,'(2i2,2i5,8i4,2e18.6)') iq, irp, ii,jj, &
                   pol%tr(:,ii), pol%tr(:,jj), &
                   real(tdummy(ii,jj),dp),aimag(tdummy(ii,jj))
           enddo
        enddo
     endif
     call MPI_BARRIER(peinf%comm,info)
  enddo
  if (peinf%master) then
     close(iunit)
     deallocate(tdummy)
  endif

  if (r_grp%rep_g(irp) == r_grp%mygr) then
     pol%lv = .false.
     deallocate(pol%zv)
  endif

  call MPI_BARRIER(peinf%comm,info)

  return

end subroutine zwrite_pol
!===================================================================
