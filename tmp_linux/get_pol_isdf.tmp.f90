












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Group master reads TDLDA eigenvectors for representation irp
! from pol_diag.dat and distributed them among all PEs in the group.
!
! OUTPUT:
!   pol%nn = nn
!   pol%ltv = .true.
!   pol%tv : allocated and read from file pol_diag.dat
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine dget_pol_isdf(pol,iunit,nrep,nq,irp,iq,nn)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'

  ! arguments
  ! polarizability structure
  type (polinfo), intent(inout) :: pol
  integer, intent(in) :: &
       iunit, &       ! number of file pol_diag.dat
       nrep, &        ! number of representations
       nq, &          ! number of q-vectors
       irp, &         ! index of current representation
       iq, &          ! index of current q-vector
       nn             ! number of eigenpairs per processor (local variable)


  ! local variables
  character (len=800) :: lastwords
  ! counters and temporary arrays
  integer :: ii, jj, npol, icol, jrp, jq, ipe, pcol
  real(dp), allocatable :: tmpv(:)
  integer :: info
  integer :: status(MPI_STATUS_SIZE)

  pol%nn = nn
  allocate(pol%dv(pol%nn,pol%ntr))
  pol%lv = .true.
  pol%dv = zero
  if (r_grp%master) then
     do ii = 1, nrep * nq
        read(iunit) jrp, jq, npol
        write(6,'(a,i3,a,2i3,i8)') ' Seek representation ', irp, &
             ' in pol_diag.dat. ', jrp, jq, npol
        if (jrp == irp .and. jq == iq) goto 60
        do jj = 1, npol + 2
           read(iunit)
        enddo
        if (jrp == nrep .and. jq == nq) then
           rewind(iunit)
           read(iunit)
           read(iunit)
        endif
     enddo
     write(lastwords,*) 'ERROR: representation ', irp, ' q-vector ', &
          iq, 'not found in pol_diag.dat!'
     call die(lastwords)
60   continue
     if (npol /= pol%ntr) then
        write(lastwords,*) 'ERROR: number of transitions in ', &
             'pol_diag.dat is wrong!', npol, pol%ntr
        call die(lastwords)
     endif
     read(iunit)
     read(iunit)
  endif
  allocate(tmpv(pol%nn*r_grp%npes))
  ! for loop over all the eigenvectors (X_s)
  do icol = 1, pol%ntr 
     if (r_grp%master) then
        write(6, *) " icol = ", icol
        tmpv = zero
        read(iunit) (tmpv(ii), ii=1,pol%ntr)
     endif
     call MPI_SCATTER( tmpv, pol%nn, MPI_DOUBLE_PRECISION, &
          pol%dv(1,icol), pol%nn, MPI_DOUBLE_PRECISION, r_grp%masterid, &
          r_grp%comm, info )
  enddo
  deallocate(tmpv)

end subroutine dget_pol_isdf













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Group master reads TDLDA eigenvectors for representation irp
! from pol_diag.dat and distributed them among all PEs in the group.
!
! OUTPUT:
!   pol%nn = nn
!   pol%ltv = .true.
!   pol%tv : allocated and read from file pol_diag.dat
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zget_pol_isdf(pol,iunit,nrep,nq,irp,iq,nn)

  use typedefs
  use mpi_module
  implicit none
  include 'mpif.h'

  ! arguments
  ! polarizability structure
  type (polinfo), intent(inout) :: pol
  integer, intent(in) :: &
       iunit, &       ! number of file pol_diag.dat
       nrep, &        ! number of representations
       nq, &          ! number of q-vectors
       irp, &         ! index of current representation
       iq, &          ! index of current q-vector
       nn             ! number of eigenpairs per processor (local variable)


  ! local variables
  character (len=800) :: lastwords
  ! counters and temporary arrays
  integer :: ii, jj, npol, icol, jrp, jq, ipe, pcol
  complex(dpc), allocatable :: tmpv(:)
  integer :: info
  integer :: status(MPI_STATUS_SIZE)

  pol%nn = nn
  allocate(pol%zv(pol%nn,pol%ntr))
  pol%lv = .true.
  pol%zv = zzero
  if (r_grp%master) then
     do ii = 1, nrep * nq
        read(iunit) jrp, jq, npol
        write(6,'(a,i3,a,2i3,i8)') ' Seek representation ', irp, &
             ' in pol_diag.dat. ', jrp, jq, npol
        if (jrp == irp .and. jq == iq) goto 60
        do jj = 1, npol + 2
           read(iunit)
        enddo
        if (jrp == nrep .and. jq == nq) then
           rewind(iunit)
           read(iunit)
           read(iunit)
        endif
     enddo
     write(lastwords,*) 'ERROR: representation ', irp, ' q-vector ', &
          iq, 'not found in pol_diag.dat!'
     call die(lastwords)
60   continue
     if (npol /= pol%ntr) then
        write(lastwords,*) 'ERROR: number of transitions in ', &
             'pol_diag.dat is wrong!', npol, pol%ntr
        call die(lastwords)
     endif
     read(iunit)
     read(iunit)
  endif
  allocate(tmpv(pol%nn*r_grp%npes))
  ! for loop over all the eigenvectors (X_s)
  do icol = 1, pol%ntr 
     if (r_grp%master) then
        write(6, *) " icol = ", icol
        tmpv = zzero
        read(iunit) (tmpv(ii), ii=1,pol%ntr)
     endif
     call MPI_SCATTER( tmpv, pol%nn, MPI_DOUBLE_COMPLEX, &
          pol%zv(1,icol), pol%nn, MPI_DOUBLE_COMPLEX, r_grp%masterid, &
          r_grp%comm, info )
  enddo
  deallocate(tmpv)

end subroutine zget_pol_isdf
