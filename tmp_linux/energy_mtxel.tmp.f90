












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate energy matrix elements: kinetic energy, Hartree
! energy, local part of electron-ion energy. Hartree energy and local
! energy are implemented in non-periodic systems only.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine denergy_mtxel(gvec,wfn,rho,kvec,e_k,e_h,e_loc)

  use typedefs
  use mpi_module
  use fft_module
  use fd_module
  use psp_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! electron wavefunctions for the current k-point and spin channel
  type (wavefunction), intent(in) :: wfn
  ! electron density for the current spin channel
  real(dp), intent(in) :: rho(w_grp%nr)
  ! k-point in units of reciprocal lattice vectors
  real(dp), intent(in) :: kvec(3)
  ! matrix elements of kinetic energy, Hartree operator,
  ! and electron-ion local potential
  real(dp), dimension(wfn%nmem,wfn%nmem), intent(out) :: e_k, e_h, e_loc

  ! local variables
  character (len=800) :: lastwords
  integer :: ii, jj, idir, icol_pe, ipe, ioff, ndim
  real(dp) :: xtmp
  ! Cartesian coordinates of k-vector
  real(dp) :: kcart(3)

  real(dp), allocatable :: v_loc(:)
  real(dp), dimension(:), allocatable :: wfn_loc, wfn_lap, v_h, wfn2, zv_loc
  real(dp), dimension(:,:), allocatable :: w_distr, wfn_grad
  real(dp), dimension(:,:,:), allocatable :: grad_distr
  real(dp), external :: ddot

  call dmatvec3('N',gvec%bvec,kvec,kcart)
  !-------------------------------------------------------------------
  ! Allocate data.
  !
  allocate(w_distr(w_grp%ldn,w_grp%npes),stat=ii)
  call alccheck('w_distr','energy_mtxel',w_grp%ldn*w_grp%npes,ii)
  w_distr = zero
  allocate(wfn_loc(w_grp%nr),stat=ii)
  call alccheck('wfn_loc','energy_mtxel',w_grp%nr,ii)
  allocate(wfn_lap(w_grp%nr),stat=ii)
  call alccheck('wfn_lap','energy_mtxel',w_grp%nr,ii)
  ndim = gvec%nr * gvec%syms%ntrans
  allocate(wfn_grad(3,ndim),stat=ii)
  call alccheck('wfn_grad','energy_mtxel',3*ndim,ii)
  allocate(grad_distr(3,w_grp%ldn,w_grp%npes),stat=ii)
  call alccheck('grad_distr','energy_mtxel',3*w_grp%ldn*w_grp%npes,ii)
  allocate(wfn2(w_grp%mydim),stat=ii)
  call alccheck('wfn2','energy_mtxel',w_grp%mydim,ii)

  call dinitialize_FFT(peinf%inode,fft_box)

  !-------------------------------------------------------------------
  ! Calculate Hartree potential (non-periodic systems only).
  !
  allocate(v_h(w_grp%nr))
  v_h = zero
  if (gvec%per == 0) then
!     if (gvec%per == 1) then
!        call dcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,kvec(1),gvec,fft_box)
!     elseif (gvec%per == 2) then
!        call dcreate_coul_2D(gvec%bdot,kvec(1),fft_box)
!     else
        call dcreate_coul_0D(gvec%bdot,kvec,fft_box)
!     endif
     v_h = one*rho
     call dpoisson(gvec,v_h,1)
  endif

  !-------------------------------------------------------------------
  ! Calculate local pseudo-potential (non-periodic systems only).
  !
  allocate(zv_loc(w_grp%mydim))
  zv_loc = zero
  if (gvec%per == 0) then
     allocate(v_loc(w_grp%mydim))
     v_loc = zero
     do ii = 1, type_num
        call v_local(gvec,psp(ii),v_loc)
     enddo
     zv_loc = one*v_loc
     deallocate(v_loc)
  endif

  !-------------------------------------------------------------------
  ! Start calculation of matrix elements.
  !
  e_k = zero
  e_h = zero
  e_loc = zero
  ioff = w_grp%offset + 1
  do icol_pe = 1, wfn%nmem, peinf%npes
     w_distr = zero
     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        if (wfn%imap(ii) == 0) then
           write(lastwords,*) ' ERROR in energy_mtxel: could not find ', &
                'wavefunction ',ii,' in memory! '
           call die(lastwords)
        endif
        call dcopy(w_grp%mydim,wfn%dwf(1,ii),1,w_distr(1,ipe+1),1)
     enddo

     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        do jj = 1, wfn%nmem
           if ( wfn%irep(wfn%imap(jj)) /= wfn%irep(wfn%imap(ii)) ) cycle
           call dcopy(w_grp%mydim,wfn%dwf(1,jj),1,wfn2,1)
           call dmultiply_vec(w_grp%mydim,v_h(ioff),wfn2)
           e_h(ii,jj) = ddot(w_grp%mydim,w_distr(1,ipe+1),1,wfn2,1)
           call dcopy(w_grp%mydim,wfn%dwf(1,jj),1,wfn2,1)
           call dmultiply_vec(w_grp%mydim,zv_loc,wfn2)
           e_loc(ii,jj) = ddot(w_grp%mydim,w_distr(1,ipe+1),1,wfn2,1)
        enddo
     enddo

     call dgather(1,w_distr,wfn_loc)
     ii = icol_pe + w_grp%inode + &
          w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
     if (ii < wfn%nmem) then
        if (fd%norder < 0) then
           call dget_lap_FFT(gvec,wfn_loc,wfn_lap,wfn%jrep(0,wfn%imap(ii)))
           call dget_grad_FFT(gvec,wfn_loc,wfn_grad,wfn%jrep(0,wfn%imap(ii)))
        else
           call dget_lap_fd(gvec%syms,wfn_loc,wfn_lap,wfn%irep(wfn%imap(ii)))
           call dget_grad_fd(gvec%syms,wfn_loc,wfn_grad,wfn%irep(wfn%imap(ii)))
        endif
     endif
     call dscatter(1,w_distr,wfn_lap)
     call dscatter(3,grad_distr,wfn_grad)

     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        do jj = 1, wfn%nmem
           if ( wfn%irep(wfn%imap(jj)) /= wfn%irep(wfn%imap(ii)) ) cycle
           e_k(ii,jj) = ddot(w_grp%mydim,w_distr(1,ipe+1),1,wfn%dwf(1,jj),1)
        enddo
     enddo
  enddo

  !-------------------------------------------------------------------
  ! Done. Deallocate memory and collect data on all PEs.
  !
  call dfinalize_FFT(peinf%inode,fft_box)

  deallocate(zv_loc)
  deallocate(v_h)
  deallocate(wfn2)
  deallocate(grad_distr)
  deallocate(wfn_grad)
  deallocate(wfn_lap)
  deallocate(wfn_loc)
  deallocate(w_distr)

  ii = wfn%nmem*wfn%nmem
  xtmp = one * gvec%syms%ntrans
  call dpsum(ii,peinf%npes,peinf%comm,e_k)
  call dscal(ii,xtmp,e_k,1)
  call dpsum(ii,peinf%npes,peinf%comm,e_h)
  call dscal(ii,xtmp,e_h,1)
  call dpsum(ii,peinf%npes,peinf%comm,e_loc)
  call dscal(ii,xtmp,e_loc,1)

  return
end subroutine denergy_mtxel
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
! Calculate energy matrix elements: kinetic energy, Hartree
! energy, local part of electron-ion energy. Hartree energy and local
! energy are implemented in non-periodic systems only.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
subroutine zenergy_mtxel(gvec,wfn,rho,kvec,e_k,e_h,e_loc)

  use typedefs
  use mpi_module
  use fft_module
  use fd_module
  use psp_module
  implicit none

  ! arguments
  ! real-space grid
  type (gspace), intent(inout) :: gvec
  ! electron wavefunctions for the current k-point and spin channel
  type (wavefunction), intent(in) :: wfn
  ! electron density for the current spin channel
  real(dp), intent(in) :: rho(w_grp%nr)
  ! k-point in units of reciprocal lattice vectors
  real(dp), intent(in) :: kvec(3)
  ! matrix elements of kinetic energy, Hartree operator,
  ! and electron-ion local potential
  complex(dpc), dimension(wfn%nmem,wfn%nmem), intent(out) :: e_k, e_h, e_loc

  ! local variables
  character (len=800) :: lastwords
  integer :: ii, jj, idir, icol_pe, ipe, ioff, ndim
  complex(dpc) :: xtmp
  ! Cartesian coordinates of k-vector
  real(dp) :: kcart(3)

  real(dp), allocatable :: v_loc(:)
  complex(dpc), dimension(:), allocatable :: wfn_loc, wfn_lap, v_h, wfn2, zv_loc
  complex(dpc), dimension(:,:), allocatable :: w_distr, wfn_grad
  complex(dpc), dimension(:,:,:), allocatable :: grad_distr
  complex(dpc), external :: zdot_c

  call dmatvec3('N',gvec%bvec,kvec,kcart)
  !-------------------------------------------------------------------
  ! Allocate data.
  !
  allocate(w_distr(w_grp%ldn,w_grp%npes),stat=ii)
  call alccheck('w_distr','energy_mtxel',w_grp%ldn*w_grp%npes,ii)
  w_distr = zzero
  allocate(wfn_loc(w_grp%nr),stat=ii)
  call alccheck('wfn_loc','energy_mtxel',w_grp%nr,ii)
  allocate(wfn_lap(w_grp%nr),stat=ii)
  call alccheck('wfn_lap','energy_mtxel',w_grp%nr,ii)
  ndim = gvec%nr * gvec%syms%ntrans
  allocate(wfn_grad(3,ndim),stat=ii)
  call alccheck('wfn_grad','energy_mtxel',3*ndim,ii)
  allocate(grad_distr(3,w_grp%ldn,w_grp%npes),stat=ii)
  call alccheck('grad_distr','energy_mtxel',3*w_grp%ldn*w_grp%npes,ii)
  allocate(wfn2(w_grp%mydim),stat=ii)
  call alccheck('wfn2','energy_mtxel',w_grp%mydim,ii)

  call zinitialize_FFT(peinf%inode,fft_box)

  !-------------------------------------------------------------------
  ! Calculate Hartree potential (non-periodic systems only).
  !
  allocate(v_h(w_grp%nr))
  v_h = zzero
  if (gvec%per == 0) then
!     if (gvec%per == 1) then
!        call zcreate_coul_1D(peinf%inode,peinf%npes,peinf%comm,kvec(1),gvec,fft_box)
!     elseif (gvec%per == 2) then
!        call zcreate_coul_2D(gvec%bdot,kvec(1),fft_box)
!     else
        call zcreate_coul_0D(gvec%bdot,kvec,fft_box)
!     endif
     v_h = zone*rho
     call zpoisson(gvec,v_h,1)
  endif

  !-------------------------------------------------------------------
  ! Calculate local pseudo-potential (non-periodic systems only).
  !
  allocate(zv_loc(w_grp%mydim))
  zv_loc = zzero
  if (gvec%per == 0) then
     allocate(v_loc(w_grp%mydim))
     v_loc = zero
     do ii = 1, type_num
        call v_local(gvec,psp(ii),v_loc)
     enddo
     zv_loc = zone*v_loc
     deallocate(v_loc)
  endif

  !-------------------------------------------------------------------
  ! Start calculation of matrix elements.
  !
  e_k = zzero
  e_h = zzero
  e_loc = zzero
  ioff = w_grp%offset + 1
  do icol_pe = 1, wfn%nmem, peinf%npes
     w_distr = zzero
     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        if (wfn%imap(ii) == 0) then
           write(lastwords,*) ' ERROR in energy_mtxel: could not find ', &
                'wavefunction ',ii,' in memory! '
           call die(lastwords)
        endif
        call zcopy(w_grp%mydim,wfn%zwf(1,ii),1,w_distr(1,ipe+1),1)
     enddo

     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        do jj = 1, wfn%nmem
           if ( wfn%irep(wfn%imap(jj)) /= wfn%irep(wfn%imap(ii)) ) cycle
           call zcopy(w_grp%mydim,wfn%zwf(1,jj),1,wfn2,1)
           call zmultiply_vec(w_grp%mydim,v_h(ioff),wfn2)
           e_h(ii,jj) = zdot_c(w_grp%mydim,w_distr(1,ipe+1),1,wfn2,1)
           call zcopy(w_grp%mydim,wfn%zwf(1,jj),1,wfn2,1)
           call zmultiply_vec(w_grp%mydim,zv_loc,wfn2)
           e_loc(ii,jj) = zdot_c(w_grp%mydim,w_distr(1,ipe+1),1,wfn2,1)
        enddo
     enddo

     call zgather(1,w_distr,wfn_loc)
     ii = icol_pe + w_grp%inode + &
          w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
     if (ii < wfn%nmem) then
        if (fd%norder < 0) then
           call zget_lap_FFT(gvec,wfn_loc,wfn_lap,wfn%jrep(0,wfn%imap(ii)))
           call zget_grad_FFT(gvec,wfn_loc,wfn_grad,wfn%jrep(0,wfn%imap(ii)))
        else
           call zget_lap_fd(gvec%syms,wfn_loc,wfn_lap,wfn%irep(wfn%imap(ii)))
           call zget_grad_fd(gvec%syms,wfn_loc,wfn_grad,wfn%irep(wfn%imap(ii)))
        endif
     endif
     call zscatter(1,w_distr,wfn_lap)
     call zscatter(3,grad_distr,wfn_grad)

     do ipe = 0, w_grp%npes - 1
        ii = icol_pe + ipe + &
             w_grp%npes * w_grp%mygr + r_grp%npes * r_grp%mygr
        if (ii > wfn%nmem) cycle
        do jj = 1, wfn%nmem
           if ( wfn%irep(wfn%imap(jj)) /= wfn%irep(wfn%imap(ii)) ) cycle
           e_k(ii,jj) = zdot_c(w_grp%mydim,w_distr(1,ipe+1),1,wfn%zwf(1,jj),1)
           do idir = 1, gvec%per
              e_k(ii,jj) = e_k(ii,jj) - Zi * two * kcart(idir) * &
                   zdot_c(w_grp%mydim,grad_distr(idir,1,ipe+1),3,wfn%zwf(1,jj),1) + &
                   zone*kcart(idir)**2 * zdot_c(w_grp%mydim,wfn%zwf(1,ii),1,wfn%zwf(1,jj),1)
           enddo
        enddo
     enddo
  enddo

  !-------------------------------------------------------------------
  ! Done. Deallocate memory and collect data on all PEs.
  !
  call zfinalize_FFT(peinf%inode,fft_box)

  deallocate(zv_loc)
  deallocate(v_h)
  deallocate(wfn2)
  deallocate(grad_distr)
  deallocate(wfn_grad)
  deallocate(wfn_lap)
  deallocate(wfn_loc)
  deallocate(w_distr)

  ii = wfn%nmem*wfn%nmem
  xtmp = zone * gvec%syms%ntrans
  call zpsum(ii,peinf%npes,peinf%comm,e_k)
  call zscal(ii,xtmp,e_k,1)
  call zpsum(ii,peinf%npes,peinf%comm,e_h)
  call zscal(ii,xtmp,e_h,1)
  call zpsum(ii,peinf%npes,peinf%comm,e_loc)
  call zscal(ii,xtmp,e_loc,1)

  return
end subroutine zenergy_mtxel
!===================================================================
