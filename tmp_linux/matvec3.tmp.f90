












!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with CPLX and then again without CPLX.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without CPLX.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
!  Performs matrix-vector multiplications in 3-d arrays. This
!  should be used in place of matmul (which assumes both arguments
!  to be matrices). Input is 3x3 matrix M and vector V. Output is
!  vector MV.
!  If op='T', performs MV = transpose(M)*V
!  If op='N', performs MV = M*V
!
!  Vec and mvec can point to the same address in parent routine.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!---------------------------------------------------------------
subroutine dmatvec3(op,mat,vec,mvec)

  use myconstants
  implicit none

  ! arguments:
  !  operation: transpose(M) or M
  character (len=1), intent(in) :: op
  !  input matrix
  real(dp), intent(in) :: mat(3,3)
  !  input vector V
  real(dp), intent(in) :: vec(3)
  !  output vector MV = op(M)*V
  real(dp), intent(out) :: mvec(3)

  ! local variables:
  integer :: ii
  real(dp) :: vtmp(3), matline(3)

  !---------------------------------------------------------------

  do ii = 1, 3
     if (op == 'N') then
        matline = one*mat(ii,:)
        vtmp(ii) = dot_product(matline,vec)
     elseif (op == 'T') then
        matline = one*mat(:,ii)
        vtmp(ii) = dot_product(matline,vec)
     endif
  enddo
  mvec = vtmp

end subroutine dmatvec3
!===================================================================













!===================================================================
!
! Macros for the complex pre-processing. All source files with extension
! .F90z are pre-processed twice. Once with 1 and then again without 1.
! Most instances of capital "Z" are replaced by either lower case "z" or
! lower case "d" respectively with or without 1.
! BE CAREFUL WITH CAPITAL "Z" IN .F90z FILES !!! IT HAS SPECIAL MEANING!!!
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!-------------------------------------------------------------------
! Constants
! Implicit functions
! External functions and subroutines
! Internal functions and subroutines
! Internal arrays
!===================================================================
!===================================================================
!
!  Performs matrix-vector multiplications in 3-d arrays. This
!  should be used in place of matmul (which assumes both arguments
!  to be matrices). Input is 3x3 matrix M and vector V. Output is
!  vector MV.
!  If op='T', performs MV = transpose(M)*V
!  If op='N', performs MV = M*V
!
!  Vec and mvec can point to the same address in parent routine.
!
! Copyright (C) 2009 Murilo L. Tiago, http://users.ices.utexas.edu/~mtiago
! This file is part of RGWBS. It is distributed under the GPL v1.
!
!---------------------------------------------------------------
subroutine zmatvec3(op,mat,vec,mvec)

  use myconstants
  implicit none

  ! arguments:
  !  operation: transpose(M) or M
  character (len=1), intent(in) :: op
  !  input matrix
  real(dp), intent(in) :: mat(3,3)
  !  input vector V
  complex(dpc), intent(in) :: vec(3)
  !  output vector MV = op(M)*V
  complex(dpc), intent(out) :: mvec(3)

  ! local variables:
  integer :: ii
  complex(dpc) :: vtmp(3), matline(3)

  !---------------------------------------------------------------

  do ii = 1, 3
     if (op == 'N') then
        matline = zone*mat(ii,:)
        vtmp(ii) = dot_product(matline,vec)
     elseif (op == 'T') then
        matline = zone*mat(:,ii)
        vtmp(ii) = dot_product(matline,vec)
     endif
  enddo
  mvec = vtmp

end subroutine zmatvec3
!===================================================================
